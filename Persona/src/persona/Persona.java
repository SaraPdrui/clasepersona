
package persona;

/**
 *por 30xp:

-Añadir a la clase Persona un String "puesto". Usarlo solo para valores "ordeño","carniceria" o "gerencia".
-Añadir a la clase Persona un Float "sueldo", que expresará el sueldo en euros.
-añadir un método cuantoGana a la clase Persona, que devuelva el String "Gana"+sueldo+" €"
-añadir un método proporcionSueldo a la clase Persona, que reciba una persona por argumentos, y devuelva un float con cuántas veces gana esa persona el sueldo de this.
-Añadir un método ajustaSueldoA, que reciba un objeto Persona, y asigne el sueldo de this, poniéndolo en proporción al del objeto que recibe por parámetro. Un/a ordeñador/a debería ganar un 25% más que alguien de carnicería, y un/a gerente un 50% más que alguien de ordeño.
-Crear la clase Vaca, con Strings para nombre, número de serie y función ("ordeño" o "carniceria"), un Float peso, un int edad, y un array de float con litros ordeñados en el día.
-Crear un método asignaFunción a una vaca, de forma que si pesa más de 200kg, se asigne a carnicería, y si pesa menos, a ordeño.
-Añadir a la clase persona un Array de Vacas
-Crear en la clase persona un método quedarmeVacas(Vaca[] vac), que copie en el array interno a las vacas que corresponden a su puesto de trabajo.uthor 1DAM
 */
public class Persona {
    String nombre;
    String apellido;
    int edad;
    float altura;
    String puesto;
    float sueldo;
    Vaca[] misVacas;
    
    
    public String cuantoGana(){
        
     return "Gana: " + this.sueldo + "Euros";  
    }
    
     public float proporcionSueldo(Persona p){
        
     return this.sueldo / p.sueldo;  
    }
    public float ajustarSueldo(Persona p){
        
      if(this.puesto.equals("gerente")&& p.puesto.equals("ordeñador")){
          this.sueldo = p.sueldo * 1.50f;
      }else if(this.puesto.equals("ordeñador")&& p.puesto.equals("carnicero")){
          this.sueldo = p.sueldo * 1.25f;  
      }else if (this.puesto.equals("gerente")&& p.puesto.equals("carnicero")){
        this.sueldo = p.sueldo * 1.75f;
      }
      return this.sueldo;
    }
            
            
     public Vaca quedarmeVacas(Vaca[] todas){
         this.misVacas = new Vaca(todas.length);
     }
        
}
